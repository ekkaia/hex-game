import { Object3D, CylinderBufferGeometry, LineSegments, EdgesGeometry, MeshBasicMaterial, Mesh, Vector3, LineBasicMaterial } from "three";

export class Hex extends Object3D {
    constructor(x: number = 0, y: number = 0, z: number = 0) {
    	super();

		let geometry = new CylinderBufferGeometry(1, 1, 0.25, 6);
		let material = new MeshBasicMaterial({ color: "blue" });
		let mesh = new Mesh(geometry, material);
		this.add(mesh);
		this.position.set(x, y, z);
		console.log(this.position)

		let edges = new EdgesGeometry(geometry);
		let edgesMaterial = new LineBasicMaterial({ color: "red" });
		let lines = new LineSegments(edges, edgesMaterial);
		this.add(lines);
	}
}