import { Box3 } from "three";
import { Hex } from "./Hex";
import { Object3D } from "three";

export class Grid extends Object3D {
	private hexArray: Array<Array<Hex>> = [];

	constructor(size: number) {
		super();

		for (let i = 0; i < size; i++) {
			this.hexArray[i] = [];
			for (let j = 0; j < size; j++) {
				let tile: Hex = new Hex(Math.sqrt(3) * (i + ((j % 2 != 0) ? 0.5 : 0)), 0, 1.5 * j);
				this.add(tile);
				this.hexArray[i][j] = tile;
			}
		}
	}

	public toHex(x : number, y : number) : Hex {
		return this.hexArray[x][y];
	}
}